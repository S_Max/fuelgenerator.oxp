/* global log player*/

this.name = "FGenerator";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Fuel generator allows to synthesize fuel from energy.";
this.version = "0.1";

"use strict";

this._DEBUG = false;

this._PREFIX = "Fuel Generator: ";

this._logger = function(msg) {
	if (this._DEBUG) {
		log(this.name, msg);
	}
};

this.activated = function() {
	if (player.ship.fuel == 7) {
		player.consoleMessage(this._PREFIX + "Fuel tanks are full!");
		return;
	}
	if (player.ship.energy < 64) {
		player.consoleMessage(this._PREFIX + "Not enough energy!");
		return;
	}

	var f = player.ship.fuel + 1;
	player.ship.fuel = (f > 7) ? 7 : f;

	var sA = player.ship.aftShield - Math.random() * player.ship.maxAftShield;
	player.ship.aftShield = (sA < 0) ? 0 : sA;

	var sF = player.ship.forwardShield - Math.random() * player.ship.maxForwardShield;
	player.ship.forwardShield = (sF < 0) ? 0 : sF;

	player.ship.energy -= 64;

	player.consoleMessage(this._PREFIX + "Done!");
};