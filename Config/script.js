/* global log player*/

this.name = "FuelGenerator";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Fuel generator allows to synthesize fuel from energy.";
this.version = "0.2";

"use strict";

this._DEBUG = false;

this._PRICE = 20000;
this._EQ_FG = "EQ_FUEL_GENERATOR";
this._EQ_REMOVE = "EQ_FUEL_GENERATOR_REMOVE";

this._logger = function(msg) {
	if (this._DEBUG) {
		log(this.name, msg);
	}
};

this.playerBoughtEquipment = function(equipmentKey) {
	if (equipmentKey == this._EQ_REMOVE) {
		player.ship.removeEquipment(this._EQ_FG);
		player.ship.removeEquipment(this._EQ_REMOVE);
		player.credits += this._PRICE / 2;
	}
};