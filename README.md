Fuel Generator

By S Max

Fuel generator allows to synthesize fuel from energy.
Synthesizes 1LY fuel per 1 energy bank. Damage shields due subspace disturbances.

Available in [Oolite](http://www.oolite.org/) package manager (Equipment).

# License
This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

# Version History
## [0.2] - 2016-09-28

- Add sell generator option

## [0.1] - 2016-09-24

- Initial release
